﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using WpfApp.Annotations;

namespace WpfApp.Models
{
    internal class MainWindowModel : INotifyPropertyChanged
    {
        private string _area1 = "Область 1";
        private string _area2 = "Область 2";
        private string _area3 = "Область 3";

        public string Area1
        {
            get => _area1;
            set
            {
                if (_area1 == value)
                {
                    return;
                }

                _area1 = value;
                OnPropertyChanged(nameof(Area1));
            }
        }

        public string Area2
        {
            get => _area2;
            set
            {
                if (_area2 == value)
                {
                    return;
                }

                _area2 = value;
                OnPropertyChanged(nameof(Area2));
            }
        }

        public string Area3
        {
            get => _area3;
            set
            {
                if (_area3==value)
                {
                    return;
                }
                
                _area3 = value;
                OnPropertyChanged(nameof(Area3));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}