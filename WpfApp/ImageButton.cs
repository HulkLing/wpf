﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace WpfApp
{
    public class ImageButton : Button
    {
        private readonly Image _image;
        private readonly TextBlock _textBlock;

        public ImageButton()
        {
            var panel = new StackPanel
            {
                Orientation = Orientation.Horizontal,
                Margin = new Thickness(10)
            };


            _image = new Image {Margin = new Thickness(0, 0, 10, 0)};
            panel.Children.Add(_image);

            _textBlock = new TextBlock();
            _textBlock.VerticalAlignment = VerticalAlignment.Center;
            panel.Children.Add(_textBlock);

            Content = panel;
        }

        public string Text
        {
            get
            {
                if (_textBlock != null)
                    return _textBlock.Text;
                return string.Empty;
            }
            set
            {
                if (_textBlock != null)
                    _textBlock.Text = value;
            }
        }

        public ImageSource Image
        {
            get
            {
                if (_image != null)
                    return _image.Source;
                return null;
            }
            set
            {
                if (_image != null)
                    _image.Source = value;
            }
        }

        public double ImageWidth
        {
            get
            {
                if (_image != null)
                    return _image.Width;
                return double.NaN;
            }
            set
            {
                if (_image != null)
                    _image.Width = value;
            }
        }

        public double ImageHeight
        {
            get
            {
                if (_image != null)
                    return _image.Height;
                return double.NaN;
            }
            set
            {
                if (_image != null)
                    _image.Height = value;
            }
        }
    }
}