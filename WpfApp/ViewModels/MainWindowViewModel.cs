﻿using System.Windows.Controls;
using WpfApp.Commands;
using WpfApp.Models;

namespace WpfApp.ViewModels
{
    internal class MainWindowViewModel
    {
        private RelayCommand _areaOneClickCommand;

        public MainWindowViewModel()
        {
            Model = new MainWindowModel();
        }

        public MainWindowModel Model { get; set; }

        public RelayCommand AreaOneClickCommand
        {
            get
            {
                return _areaOneClickCommand ??
                       (_areaOneClickCommand = new RelayCommand(obj =>
                       {
                           switch (((Button) obj).Name)
                           {
                               case "Button1":
                                   Model.Area1 = ButtonClick(Model.Area1);
                                   break;
                               case "ImageButton":
                                   Model.Area2 = ButtonClick(Model.Area2);
                                   break;
                               case "Button2":
                                   Model.Area3 = ButtonClick(Model.Area3);
                                   break;
                           }
                       }));
            }
        }

        private string ButtonClick(string area)
        {
            int result;
            if (int.TryParse(area, out result))
            {
                result += 1;
                area = result.ToString();
            }
            else
            {
                area = 1.ToString();
            }
            return area;
        }
    }
}